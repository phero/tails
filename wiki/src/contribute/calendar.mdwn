[[!meta title="Calendar"]]

All times are referenced in [[!wikipedia UTC]].

# 2023 Q2

* 2023-04-18: **Release 5.12** (Firefox 102.10) — intrigeri is the RM, nodens is the TR
